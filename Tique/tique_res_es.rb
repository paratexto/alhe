#!/usr/bin/env ruby
# encoding: UTF-8
# coding: UTF-8
puts "
  *******************************************
  *******************************************
  **********  Bienvenido a Tique   **********
  *******************************************
  *******************************************
"

puts 'Comenzaremos el proceso de edición de reseñas con Tique. Indica el número del proceso que deseas y presiona enter. Considera cuidado en tu proceso. Happy Editing :)'

require "rubygems"
require "highline/import"

def copiar_archivos
  require 'fileutils'
    FileUtils.mkdir_p 'PDF'
    # => ["foo/bar"]
    directory_name = "PDF/imgs"
      Dir.mkdir(directory_name) unless File.exists?(directory_name)
  puts "directorio PDF creado"

  puts 'Copiando archivos de imágenes para PDF'

  require 'open-uri'
    open('PDF/imgs/conacyt.png', 'wb') do |file|
      file << open('https://gitlab.com/paratexto/alhe/raw/master/PDF/conversion/imgs/conacyt.png').read
  end

  open('PDF/imgs/ID.png', 'wb') do |file|
    file << open('https://gitlab.com/paratexto/alhe/raw/master/PDF/conversion/imgs/ID.png').read
  end

  open('PDF/imgs/Logo1.eps', 'wb') do |file|
   file << open('https://gitlab.com/paratexto/alhe/raw/master/PDF/conversion/imgs/Logo1.eps').read
  end

  open('PDF/imgs/Logo2.eps', 'wb') do |file|
    file << open('https://gitlab.com/paratexto/alhe/raw/master/PDF/conversion/imgs/Logo2.eps').read
  end

  open('PDF/imgs/alhelogo.png', 'wb') do |file|
    file << open('https://gitlab.com/paratexto/alhe/raw/master/PDF/conversion/imgs/alhelogo.png').read
  end

  open('PDF/imgs/licencia.png', 'wb') do |file|
    file << open('https://gitlab.com/paratexto/alhe/raw/master/PDF/conversion/imgs/licencia.png').read
  end

  open('PDF/imgs/mora.png', 'wb') do |file|
    file << open('https://gitlab.com/paratexto/alhe/raw/master/PDF/conversion/imgs/mora.png').read
  end

  open('PDF/imgs/orcid.svg', 'wb') do |file|
    file << open('https://gitlab.com/paratexto/alhe/raw/master/PDF/conversion/imgs/orcid.svg').read
  end

  puts 'imagenes para PDF copiadas correctamente'

  ### Documentos para latex necesarios

  require 'open-uri'
  open('PDF/resenaplantisha.txt', 'wb') do |file|
    file << open('https://gitlab.com/paratexto/alhe/raw/master/Resenas/convertion/resenaplantisha.txt').read
  end

  open('PDF/meta.yaml', 'wb') do |file|
    file << open('https://gitlab.com/paratexto/alhe/raw/master/Resenas/convertion/meta.yaml').read
  end

  puts "plantilla de metadatos copiada"
  puts "plantilla latex copiada"

  system('clear')
  system("ls -a")

  puts '

  ======Archivos correctamente copiados y organizados. Indica el número del proceso que deseas y presiona enter. Considera cuidado en tu proceso.======
  '
end

def editar_metadata

    puts 'Abriendo plantilla de metadatos, favor de llenarla correctamente. Una vez llenada el proceso de Tique continuará.'

    system('atom', 'PDF/meta.yaml')

    until File.exist?("PDF/meta.yaml")
      sleep 1
    end

    require 'highline/import' # gem install highline
    def yesno(prompt = 'Continue?', default = true)
      a = ''
      s = default ? '[Y/n]' : '[y/N]'
      d = default ? 'y' : 'n'
      until %w[y n].include? a
        a = ask("#{prompt} #{s} ") { |q| q.limit = 1; q.case = :downcase }
        a = d if a.length == 0
      end
      a == 'y'
    end

    puts yesno("¿Se ha editado satisfactoriamente la plantilla de metadatos?", true)

    system('clear')

  puts '

  ======Asegúrate de que tu archivo de metadata está actualizado. Indica el número del proceso que deseas y presiona enter. Considera cuidado en tu proceso.======
  '
end

def docto_latex
  puts 'Creando documento LaTeX para edición de reseña...'
    require 'fileutils'
    FileUtils.cp Dir.glob("./*.{docx}"), "./PDF"

    puts 'Creando archivo TeX para su edición'

    system('pandoc PDF/*.docx -t markdown -o PDF/resena.md --include-in-header=PDF/meta.yaml')

    system('pandoc PDF/*.md -o PDF/resena.tex --template=PDF/resenaplantisha.txt')

    system('clear')

  puts '

  ======Archivo LaTeX creado satisfactoriamente. Indica el número del proceso que deseas y presiona enter. Considera cuidado en tu proceso.======
  '
end

def editar_latex
    puts ('Abriendo documento TeX para editarlo')

    system('texstudio', 'PDF/resena.tex')

    until File.exist?("PDF/resena.tex")
      sleep 1
    end

    require 'highline/import' # gem install highline
    def yesno(prompt = 'Continue?', default = true)
      a = ''
      s = default ? '[Y/n]' : '[y/N]'
      d = default ? 'y' : 'n'
      until %w[y n].include? a
        a = ask("#{prompt} #{s} ") { |q| q.limit = 1; q.case = :downcase }
        a = d if a.length == 0
      end
      a == 'y'
end

    puts yesno("¿Se ha editado satisfactoriamente docuemnto Tex?", true)

    system('clear')

  puts '

  ======Asegúrate de que tu archivo LaTeX está actualizado. Indica el número del proceso que deseas y presiona enter. Considera cuidado en tu proceso.======
  '
end

def crear_html
    puts 'Creando archivo html para su edición...'

    system ('pandoc -s PDF/resena.tex -o resena.html')
    system('clear')
        system ('ls -l')

  puts '

  ======Archivo HTML creado satisfactoriamente. Indica el número del proceso que deseas y presiona enter. Considera cuidado en tu proceso.======
  '
end

def edicion_html
    puts ('Abriendo documento HTML para editarlo')

    system('atom', 'resena.html')

    until File.exist?("resena.html")
      sleep 1
    end

    require 'highline/import' # gem install highline
    def yesno(prompt = 'Continue?', default = true)
      a = ''
      s = default ? '[Y/n]' : '[y/N]'
      d = default ? 'y' : 'n'
      until %w[y n].include? a
        a = ask("#{prompt} #{s} ") { |q| q.limit = 1; q.case = :downcase }
        a = d if a.length == 0
      end
      a == 'y'
    end

    puts yesno("¿Se ha editado satisfactoriamente docuemnto HTML?", true)

    system('clear')

  puts '

  ======Asegúrate de que tu archivo HTML está actualizado. Indica el número del proceso que deseas y presiona enter. Considera cuidado en tu proceso.======
  '
end

begin
  puts
  loop do
    choose do |menu|
      menu.prompt = "Por favor, escoge la opción de tu proceso de edición"
      menu.choice(:Copiar_Archivos) { copiar_archivos() }
      menu.choice(:Edición_Metadata) { editar_metadata() }
      menu.choice(:Creación_de_archivo_LaTeX) { docto_latex() }
      menu.choice(:Edición_de_archivo_LaTeX) { editar_latex() }
      menu.choice(:Creación_de_archivo_HTML) { crear_html() }
      menu.choice(:Edición_de_archivo_HTML) { edicion_html() }
      menu.choice(:Salir, "Exit program.") { (exit) }
    end
  end
end
