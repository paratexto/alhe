#!/usr/bin/env ruby
# encoding: UTF-8
# coding: UTF-8
system('clear')

puts "
  *******************************************
  *******************************************
  **********  bem-vindo a Tique   ***********
  *******************************************
  *******************************************
"

puts 'Iniciaremos o processo de edição de artigos acadêmicos com Tique. Indique o número do processo que deseja e pressione enter. Tenha cuidado em seu processo. Happy Editing :)'

require "rubygems"
require "highline/import"


def preparar_archivos
  # Renombrar imágenes
  puts "Renomeando arquivos ..."

  require 'fileutils'

  product_name  = "-g";
  id = 1;
  extension = ".eps"

  filenames = Dir.glob("*.{eps,png,tiff,tif,jpg,jpeg,gif}")

  filenames.each do |filename|
      File.rename(filename, File.expand_path('../', __FILE__) + product_name + id.to_s + extension)
      id += 1
  end

  # Ranombrar Bibliografía

  require 'fileutils'

  product_name = "biblio";

  filenames = Dir.glob("*.bib")

  filenames.each do |filename|
    File.rename(filename, product_name + '.bib')
  end

  # Renombrar documento base

  require 'fileutils'

  filenames = Dir.glob("*.docx")

  filenames.each do |filename|
      File.rename(filename, File.expand_path('../', __FILE__) + '.docx')
  end

  require 'fileutils'

  puts "Arquivos renomeados."
  # Creación de directorio para carpate DIGITAL y PDF
  directory_name = "DIGITAL"
  Dir.mkdir(directory_name) unless File.exists?(directory_name)
  # Elimina archivos copiados innecesarios
  require 'fileutils'
  FileUtils.cp Dir.glob("../*.{eps,docx}"), "./"
  FileUtils.rm Dir.glob('../*.{eps,docx}')

  ############
  #Copiar archivos a carpetas
  require 'fileutils'
  dont_copy = ['jquery.languageTags.js']
  puts "Copiando Archivos originales"
  from_dir = "./"
  to_dir = "./DIGITAL"
  contains = Dir.new(from_dir).entries
  def copy_with_path(src, dst)
    FileUtils.mkdir_p(File.dirname(dst))
    FileUtils.cp(src, dst)
  end
  Dir[from_dir + "/**/*.{docx,jpg,jpeg,gif,png,eps,ia,bib,rtf}"].each do |old_dest|
    new_dest = old_dest.gsub(from_dir, to_dir)

    # nueva carpeta destino
    should_not_copy = dont_copy.any? { |s| new_dest.end_with?(s) }

    if !should_not_copy
      puts new_dest
      copy_with_path(old_dest, new_dest);
    end
  end
  puts '¡Sucesso ao copiar arquivos!'

  ####
  # Creación de imágenes png
  system('mogrify -format png -colorspace sRGB -background transparent -resize 1200 -define profile:skip="*" *.eps')
  ## Borra imágenes eps del directorio DIGITAL
  FileUtils.rm Dir.glob('./DIGITAL/*.eps')
  # Crea carpeta de links, donde van las imágenes del artículo
  require 'fileutils'
  FileUtils.mkdir_p 'PDF/links'
  #Copia las imágenes eps del directorio raíz a la carpeta de links y directorio para las imágenes instituconales.
  require 'fileutils'
  FileUtils.cp Dir.glob("*.eps"), "PDF/links"
  Dir.mkdir('PDF/imgs') unless Dir.exist?('PDF/imgs')
  # Crear carpeta de archivos base
  directory_name = "Arquivos_base"
  Dir.mkdir(directory_name) unless Dir.exists?(directory_name)
  # Copia los archivo originales a la carpeta de respaldo
  FileUtils.cp Dir.glob("*.{docx,bib,rtf,png}"), "./Arquivos_base/"
  FileUtils.rm Dir.glob('./*.{eps,docx,bib,rtf,png}')
  FileUtils.cp Dir.glob("./Arquivos_base/*.{png}"), "./DIGITAL/"
  ##Descarga de imágenes necesarias para el pdf en la carpeta PDF/imgs
  puts 'Copiar arquivos necessários para edição'

# Copia de archivos necesarios para PDF
    require 'fileutils'
    FileUtils.cp Dir.glob("*.eps"), "PDF/links"

   require 'open-uri'
    URI.open('PDF/imgs/conacyt.png', 'wb') do |file|
      file << URI.open('https://gitlab.com/paratexto/alhe/raw/master/PDF/conversion/imgs/conacyt.png').read
    end

    URI.open('PDF/imgs/ID.png', 'wb') do |file|
      file << URI.open('https://gitlab.com/paratexto/alhe/raw/master/PDF/conversion/imgs/ID.png').read
    end

    URI.open('PDF/imgs/Logo1.eps', 'wb') do |file|
      file << URI.open('https://gitlab.com/paratexto/alhe/raw/master/PDF/conversion/imgs/Logo1.eps').read
    end

    URI.open('PDF/imgs/Logo2.eps', 'wb') do |file|
      file << URI.open('https://gitlab.com/paratexto/alhe/raw/master/PDF/conversion/imgs/Logo2.eps').read
    end

    URI.open('PDF/imgs/alhelogo.png', 'wb') do |file|
      file << URI.open('https://gitlab.com/paratexto/alhe/raw/master/PDF/conversion/imgs/alhelogo.png').read
    end

    URI.open('PDF/imgs/licencia.png', 'wb') do |file|
      file << URI.open('https://gitlab.com/paratexto/alhe/raw/master/PDF/conversion/imgs/licencia.png').read
    end

    URI.open('PDF/imgs/mora.png', 'wb') do |file|
      file << URI.open('https://gitlab.com/paratexto/alhe/raw/master/PDF/conversion/imgs/mora.png').read
    end

    URI.open('PDF/imgs/orcid.svg', 'wb') do |file|
      file << URI.open('https://gitlab.com/paratexto/alhe/raw/master/PDF/conversion/imgs/orcid.svg').read
    end

    URI.open('PDF/plantilla_latex_pt.txt', 'wb') do |file|
      file << URI.open('https://gitlab.com/paratexto/alhe/raw/master/PDF/conversion_pt/plantilla_latex_pt.txt').read
    end

    require 'open-uri'
    URI.open('DIGITAL/plantilla.yaml', 'wb') do |file|
      file << URI.open('https://gitlab.com/paratexto/alhe/raw/master/XML/%20XML_to_HTML%20_pt/plantilla_pt.yaml').read
    end

require 'open-uri'
URI.open('DIGITAL/jats3.cls', 'wb') do |file|
  file << URI.open('https://gitlab.com/paratexto/alhe/raw/master/XML/%20XML_to_HTML%20_pt/Convertion/jats3.csl').read
end

require 'open-uri'
URI.open('DIGITAL/jats.lua', 'wb') do |file|
  file << URI.open('https://gitlab.com/paratexto/alhe/raw/master/XML/%20XML_to_HTML%20_pt/Convertion/jats.lua').read
end

require 'open-uri'
URI.open('DIGITAL/default.jats', 'wb') do |file|
  file << URI.open('https://gitlab.com/paratexto/alhe/raw/master/XML/%20XML_to_HTML%20_pt/Convertion/default_pt.jats').read
end

puts 'default jats copiado'
puts 'os arquivos necessários para XML foram copiados com sucesso '
system ('clear')
end  # este es el end de prerar_archivos

def editar_metadata

  puts 'Abrindo modelo de metadados, preencha-o corretamente. Depois de preenchido, o processo de Ingresso continuará.'

  system('atom', 'DIGITAL/plantilla.yaml')

  until File.exist?("DIGITAL/plantilla.yaml")
    sleep 1
  end

  require 'highline/import' # gem install highline
  def yesno(prompt = 'Continue?', default = true)
    a = ''
    s = default ? '[Y/n]' : '[y/N]'
    d = default ? 'y' : 'n'
    until %w[y n].include? a
      a = ask("#{prompt} #{s} ") { |q| q.limit = 1; q.case = :downcase }
      a = d if a.length == 0
    end
    if a == 'y' or a == 'yes'
      return true
  elsif a == 'n' or a == 'no'
      return false
  else
  end
  end

  puts yesno("O modelo de metadados foi editado com sucesso? ", true)
  system ('clear')
  puts 'metadados salvos'
end

######## Conversión de Archivos

def conversion_archivos

  puts 'Convertendo arquivos'

  system('pandoc -s DIGITAL/*.docx -t markdown -o DIGITAL/doc.md')

  system('pandoc DIGITAL/*.md -o DIGITAL/doc.html')

  system('pandoc -s DIGITAL/*.html -t markdown -o DIGITAL/doc.md --include-in-header=DIGITAL/plantilla.yaml  --markdown-headings=atx')

  require 'fileutils'

  # Variables
  preeliminar = 'DIGITAL/doc.md'

  # Lee el archivo de la tesis
  md     = File.read(preeliminar)
  blocks = md.split(/\n{2,}/)
  clean  = [blocks.first]

  # Iteración de cada bloque
  blocks.each_with_index do |b, i|
    # Si no es el primer bloque
    if i > 0
      # Si es bloque de cita
      if b =~ /^>/
        clean.push("```\n" + b.gsub('>', '').split(/\n/).join(' ') + "\n```")
      # Si es un párrafo
      elsif b =~ /^[\w|\s]/
        clean.push(b.split(/\n/).join(' '))
      # Resto
      else
        clean.push(b)
      end
    end
  end

  # Guarda docto preeliminar
  file = File.open(preeliminar, 'w:utf-8')
  file.puts clean.join("\n\n") # OJO: une de nuevo los bloques con dos saltos de línea
  file.close

  puts ':)'

  puts ':)'

  system('clear')
end

def marcacion_articulo

    ##### Escrip que perro hizo :3

    require 'fileutils'

    # Variables
    preeliminar = 'DIGITAL/doc.md'

    # Lee el archivo de la tesis
    md = File.read(preeliminar)

    # Translitera el nombre del autor
    def transliterate string
      # Seguir llenando
      dic1 = 'áäãâéêíóõôúüñç'
      dic2 = 'aaaaeeiooouunc'

      return string.downcase.tr(dic1, dic2).capitalize
    end

    # Procesa las referencias
    def process e, paren = false
      refs  = e.gsub(/[\(|\)]/, '').split(/;\s*/)
      clean = []

      refs.each do |r|
        author  = r.gsub(/^([A-ZÀ-ÚÇç][a-zA-Z\u00C0-\u024FÇç]+),{0,1}\s+.*$/, '\1')
        year    = r.gsub(/\D+(\d{4}\w{0,1}).*$/, '\1')
        pages_r = r.gsub(/^.+\,\s+(p{1,2}\.\s+[\d|-]+$)/, '\1')
        pages_c = pages_r =~ /^p{1,2}\./ ? '**' + pages_r + '**' : ''

        if paren
          clean.push('[@' + transliterate(author) + year + ']' + pages_c)
        else
          clean.push('@' + transliterate(author) + year + pages_c)
        end
      end

      return clean.join('; ')
    end

    # Limpia la tesis
    clean = md.gsub(/[A-ZÀ-ÚÇç][a-zA-Z\u00C0-\u024FÇç]+\s+\(\d{4}\w{0,1}.*?\)/, ) do |e|
                process(e)
              end
               .gsub(/\([A-ZÀ-ÚÇç][a-zA-Z\u00C0-\u024FÇç]+,\s+\d{4}\w{0,1}.*?\)/, ) do |e|
                process(e, true)
              end

    # Guarda la tesis
    file = File.open(preeliminar, 'w:utf-8')
    file.puts clean
    file.close

    # Corregir elementos como corchetes en llamado a referencias
file_names = ['DIGITAL/doc.md']

    file_names.each do |file_name|
      text = File.read(file_name)
      alt_corchetes = text.gsub(/\];\s*\[/, '; ')

      #puts new_contents

      # Para escribir los cambios se usa:
      File.open(file_name, "w") {|file| file.puts alt_corchetes }
    end
    system ('ls -a')

    puts 'Abrindo arquivo para marcação. O ingresso continuará seu processo quando o item terminar de marcar ...'

    system('atom', 'DIGITAL/doc.md')

    require 'highline/import' # gem install highline
    def yesno(prompt = 'Continue?', default = true)
      a = ''
      s = default ? '[Y/n]' : '[y/N]'
      d = default ? 'y' : 'n'
      until %w[y n].include? a
        a = ask("#{prompt} #{s} ") { |q| q.limit = 1; q.case = :downcase }
        a = d if a.length == 0
      end
      a == 'y'
    end

    puts yesno("O documento foi marcado? ", true)

    system ('clear')
end
def conversion_XMLJats

    puts 'Convirtiendo archivo a XML-Jats'

    system('pandoc DIGITAL/*.md --filter pandoc-citeproc -t DIGITAL/jats.lua -o DIGITAL/doc.xml --template=DIGITAL/default.jats --bibliography=DIGITAL/biblio.bib --csl=DIGITAL/jats3.cls')

    puts 'Arquivo convertido com sucesso '
    puts '..... Corrigindo outras inconsistências em XML'

  # Corregir elementos como & y llamados a notras en xml
  file_names = ['DIGITAL/doc.xml']

  file_names.each do |file_name|
         text = File.read(file_name)
         ampersan = text.gsub(/\s\&\s/, ' e ')

      File.open(file_name, "w") {|file| file.puts ampersan }
    end

    # Llamados a nota

  puts 'Corrigindo chamadas para nota ... bip bip bip'

  file_names.each do |file_name|
    text = File.read(file_name)
    notas1 = text.gsub(/\[\^\d{1,3}\^\]\(#fn/, '<xref ref-type="fn" rid="fn')

    File.open(file_name, "w") {|file| file.puts notas1 }
    end

    file_names.each do |file_name|
      text = File.read(file_name)
      notas2 = text.gsub(/\)\{#fnref/, '"><sup>')

    File.open(file_name, "w") {|file| file.puts notas2 }
    end

    file_names.each do |file_name|
      text = File.read(file_name)
      notas3 = text.gsub(/\s\.footnote-ref\}/, '</sup></xref>')

    File.open(file_name, "w") {|file| file.puts notas3 }
    end
# notas
    file_names.each do |file_name|
      text = File.read(file_name)
      fngroup = text.gsub(/\Dlist\s*\w{4}\D\w{4}\D{2}\w{5}\D{1}>/, '<fn-group>')


    File.open(file_name, "w") {|file| file.puts fngroup }
    end

    file_names.each do |file_name|
      text = File.read(file_name)
      fnlist = text.gsub(/(\Dlist\D\w{4}\D{2}\w\D{3})(fn\d{1,3})(\})/, '<fn fn-type="other" id="\2">')


    File.open(file_name, "w") {|file| file.puts fnlist }
    end

    file_names.each do |file_name|
      text = File.read(file_name)
      fnlist2 = text.gsub(/(<fn\s.*?>)(\s)/, '\1 <label></label> <p>')


    File.open(file_name, "w") {|file| file.puts fnlist2 }
    end

    file_names.each do |file_name|
      text = File.read(file_name)
      fnlist3 = text.gsub(/(<fn\s.*<\/p>)(<\/\w*\D\w*>)/, '\1 </fn>')


    File.open(file_name, "w") {|file| file.puts fnlist3 }
    end

    file_names.each do |file_name|
      text = File.read(file_name)
      fngroup2 = text.gsub(/(<\/fn>\n*\s*)(<\/list>)/, '\1 </fn-group>')


    File.open(file_name, "w") {|file| file.puts fngroup2 }
    end

    file_names.each do |file_name|
      text = File.read(file_name)
      fngroup3 = text.gsub!(/(<fn-group>\n*\s*)/, '\1 <title>Notas</title>')


    File.open(file_name, "w") {|file| file.puts fngroup3 }
    end

    ###########referencias

  puts 'Correção básica de referências. Guau guau'

    file_names.each do |file_name|
      text = File.read(file_name)
      refs = text.gsub!(/(<p>)(<ref.*.*?)(<\/p>)/, '\2')

    File.open(file_name, "w") {|file| file.puts refs }
    end

    file_names.each do |file_name|
      text = File.read(file_name)
      refs2 = text.gsub(/(<mixed-\w{8}>)(.*)(<\/mixed-\w{8}>)/, '\1\3')

    File.open(file_name, "w") {|file| file.puts refs2 }
    end

    file_names.each do |file_name|
      text = File.read(file_name)
      refs3 = text.gsub(/<\/surname>,/, '</surname>')

      File.open(file_name, "w") {|file| file.puts refs3 }
      end

      file_names.each do |file_name|
        text = File.read(file_name)
        refs3 = text.gsub(/<\/name><\/name><name>/, '</name>')

        File.open(file_name, "w") {|file| file.puts refs3 }
        end

        ## Otras conversion_archivos

        puts 'Corrigindo outras coisas, miauuu'

        file_names.each do |file_name|
          text = File.read(file_name)
          cita = text.gsub(/<pre><code>/, '<disp-quote><p>')

          File.open(file_name, "w") {|file| file.puts cita }
          end

          file_names.each do |file_name|
            text = File.read(file_name)
            cita2 = text.gsub(/<\/code><\/pre>/, '</p></disp-quote>')

            File.open(file_name, "w") {|file| file.puts cita2 }
            end

            file_names.each do |file_name|
              text = File.read(file_name)
              corchetes_cita = text.gsub(/\\\[/, '[')

              File.open(file_name, "w") {|file| file.puts corchetes_cita }
            end

            file_names.each do |file_name|
              text = File.read(file_name)
              corchetes_cita = text.gsub(/\\\]/, ']')

              File.open(file_name, "w") {|file| file.puts corchetes_cita }
            end
  end
def marcacion_xmljats
    puts 'Abrindo arquivo para marcação para XML Jats. O ingresso continuará seu processo quando o item terminar de marcar ...'

    system('/home/leon/Oxygen XML Editor 20/oxygen20.0', 'DIGITAL/doc.xml')

    require 'highline/import' # gem install highline
    def yesno(prompt = 'Continue?', default = true)
      a = ''
      s = default ? '[Y/n]' : '[y/N]'
      d = default ? 'y' : 'n'
      until %w[y n].include? a
        a = ask("#{prompt} #{s} ") { |q| q.limit = 1; q.case = :downcase }
        a = d if a.length == 0
      end
      a == 'y'
    end

    ################
    require 'fileutils'

    filenames = Dir.glob('DIGITAL/*.md')

    filenames.each do |filename|
        File.rename(filename, File.expand_path('../', __FILE__) + '.md')
    end
end

def archivo_latex
  puts 'Criação do arquivo Latex do artigo'

  require 'base64'
  cmd = %{Get-ChildItem -Path DIGITAL\ -File -Recurse -Filter *.docx | ForEach-Object { pandoc -o ($_.FullName + ".md") $_.FullName --include-in-header=DIGITAL/plantilla.yaml --markdown-headings=atx }}
  encoded_cmd = Base64.strict_encode64(cmd.encode('utf-16le'))
  find = `powershell.exe -encodedCommand #{encoded_cmd}`

require 'fileutils'
FileUtils.cp Dir.glob("./DIGITAL/*.md"), "./PDF/"

require 'base64'
cmd = %{Get-ChildItem -Path PDF\ -File -Recurse -Filter *.md | ForEach-Object { pandoc -o ($_.FullName + ".tex") $_.FullName --template=PDF/plantilla_latex_pt.txt }}
encoded_cmd = Base64.strict_encode64(cmd.encode('utf-16le'))
find = `powershell.exe -encodedCommand #{encoded_cmd}`

File.delete("./PDF/*.md") if File.exist?("./PDF/*.md")

  puts 'Arquivo LaTeX criado com sucesso !!'

end

def editar_latex
    puts ('Abrindo documento TeX para edição')

    system('texstudio', 'PDF/*.tex')

    until File.exist?("PDF/*.tex")
      sleep 1
    end

    require 'highline/import' # gem install highline
    def yesno(prompt = 'Continue?', default = true)
      a = ''
      s = default ? '[Y/n]' : '[y/N]'
      d = default ? 'y' : 'n'
      until %w[y n].include? a
        a = ask("#{prompt} #{s} ") { |q| q.limit = 1; q.case = :downcase }
        a = d if a.length == 0
      end
      a == 'y'
end

    puts yesno("O documento Tex foi editado com sucesso?", true)

    system('clear')
end

##### Inicio del menú
begin
  puts
  loop do
    choose do |menu|
      menu.prompt = "Escolha a opção para o seu processo de edição"
      menu.choice(:Preparar_Archivos) { preparar_archivos() }
      menu.choice(:Edición_Metadata) { editar_metadata() }
      menu.choice(:Conversion_de_archivos) { conversion_archivos() }
    #  menu.choice(:Aplicación_de_políticas) { politicas_editoriales() }
      menu.choice(:Marcación_de_archivo_MD) { marcacion_articulo() }
      menu.choice(:Conversión_de_archivo_MD_a_XML) { conversion_XMLJats() }
      menu.choice(:Marcación_de_archivo_XML) { marcacion_xmljats() }
      menu.choice(:Creación_de_archivo_LaTeX) { archivo_latex() }
      menu.choice(:Edición_de_archivo_LaTeX) { editar_latex() }
    #  menu.choice(:Fin_del_preoceso) { renombrar_archivos() }
      menu.choice(:Salir, "Exit program.") { (exit) }
    end
  end
end
