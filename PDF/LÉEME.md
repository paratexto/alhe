# Manual para elaboración de PDF de *ALHE*

Proceso sugerido para la elaboración de archivos LaTeX de los artículos aceptados en *ALHE* para su versión impresa. 

Esta forma de edición se ha trabajado desde 2017, en un primer momento para los *Preprints*, y a partir de 2019 para la formación de los documentos PDF que serán distribuidos en el portal de [*ALHE*](alhe.mora.edu.mx).

Dada la necesidad de adaptación a los nuevos esquemas y estándares de la publicación científica a escala internacional, *ALHE* adopto el modelo de publicación continua, con el fin de seguir siendo un referente regional en cuanto a la calidad e innovación académica. 

Derivado de estas necesidades, el flujo de trabajo que hasta 2018 se mantuvo con el Departamento de Publicaciones del Instituto Mora cambió, pues la celeridad de publicación de los artículos era prioritaria. Con esto, la maquetación de las versiones PDF de cada número queda en manos de la redacción de la Revista y es supervisada por el Departamento de Producción Editorial.

La propuesta que realiza *ALHE* es la maquetación de los archivos por medio del lenguaje de composición de textos [LaTeX](https://es.wikipedia.org/wiki/LaTeX), dada su interoperabilidad, estabilidad y fácil acceso. 

Resulta indispensable tener en mente que esta metodología **no es** una solución definitiva a la edición PDF, sino un puente en construcción al que aún le queda muchísimo por hacer.

Entre las tareas truncas que aún se investigan y están por hacerse son la resolución de dudas respecto cuadros, mayor facilidad para la edición de referencias.

## Proceso

### Requerimentos

* Archivo `md` utilizado para formar el `xml`
* Archivo `plantilla_latex.txt`. 
* Directorio `imgs` que contiene las´imágenes institucionales.
* Directorio `links` que contiene las imágenes del artículo.

### Primer proceso: Preparación en Markdown

Si utilizamos este proceso requeriremos el documento Markdown **antes** de la marcación de referencias, es decir solo con la cabecera `yaml` editada. De esta forma la matadata necesesaria estará integrada a nuestro documento.

1. En la carpeta del artículo crear un nuevo directorio llamado `PDF`
2. Realizar una copia de la carpeta `imgs` de [este link](https://gitlab.com/paratexto/alhe/tree/master/PDF/conversion/imgs) 
3. En caso de que el artículo requiera imágenes crear una carpeta llamada `links`
4. Ejecutar en una terminal el comando 

~~~
pandoc -s tuarchivo.md -o tuarchivo.tex --template=plantilla_latex.txt
~~~

5. En el archivo resultante borrar los metadatos repetidos.[^1]
6. Verificar plecas.
7. Editar los meses de las fechas de recepción, etc.
8. Editar las etiquetas `\section{}` a `\section*{}` 
9. Agregar `\noindent` sepues de cada sección y subsección.
10. se editan las imágenes como gráficas o figuras:

~~~
	\afterpage{
	\begin{grafica}
		\label{g1}			
		\centering
		\caption{\uppercase{Título}}
		\includegraphics[width=.95\textwidth]{links/nombre_imagen}\\
		\justify \hspace{6.5mm}\footnotesize Fuentes: Referencia.	
	\end{grafica}
}
~~~

11. Los cuadros se pueden editar con la ayuda de [Latex Table Generator](https://www.tablesgenerator.com/latex_tables)
12. La lista (o listas) de referencias se colocan en el entorno `\begin{hangparas}{6.2mm}{1} \end{hangparas}`. 

## Notas

[^1]: En caso de que exista más de un autor es necesario completarlos manualmente.
