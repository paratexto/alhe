# ALHE

GIT de procesos digitales de ALHE para XML Jats y flujos de trabajo.

Dentro de este Git encontrarás los métodos de trabajo empleados para la edición de la revista *[América Latina en la Historia Económica](alhe.mora.edu.mx)*, editada por el Instituto de Investigaciones Dr. José María Luis Mora.

La edición de los artículos aceptados para su publicación se divide en tres rubros:

1. Edición PDF para impresión
2. Edición XML Jats SPS
3. Edición HTML y Epub

Cada uno de estos tres rubros debe trabajarse con base en una metodología híbrida, y cuyos resultados no planeados devinieron en un acercamiento más meticuloso hacia el texto, su forma de edición y la compresión de su materialización en el mundo digital. De ahí que estos procesos te ayudarán a conocer los textos editados por la revista *ALHE* en sus entrañas.

Este manual ha sido realizado por Alberto R. León ([intratextual](https://twitter.com/intratextual)). 

Contacto: mrleon@riseup.net

Última edición 7, oct. 2019.
