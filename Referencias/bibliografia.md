# Sobre la bibliografía

Para corroborar la bibliografía se puede, opcionalmente, hacer de la siguiente manera:

1. Descargar el documento `referencia.md` 
2. En bash utilizar el comando:

```
$ pandoc --filter=pandoc-citeproc --csl=estilo.csl --standalone referencias.md -o referencias.docx

```

3. Se revisa el documento `docx`, si requiere cambios se editan y se guardan.
4. Una vez revisado el documento, asegurándonos de que es correcto, convertimos el `.docx` a `.md` utilizando `$ pandoc -s -S referencias.docx -o nombre_del_archivo_.md`
5. Este documento, en caso de ser necesario, reemplazará la lista de referencias del documento `md` del artículo.