# Manual para elaboración de Reseñas ALHE

***Es necesario que nuestro documento se encuentre en formato .docx***

## Requerimientos 

-   Documento en formato `docx`.
-   El archivo `tique_res_es.rb`, puede descargarse de [este link](https://gitlab.com/paratexto/alhe/blob/master/Tique/tique_res_es.rb).
-   Tener instalado [Atom](https://atom.io).
-   Tener instalado [TeXStudio](https://www.texstudio.org).
-   Haber registrado la reseña en el módulo *Quick Submit* de OJS, para obtener el número DOI.

## Proceso 

1. Asegurarse que la reseña tenga una carpeta expecífica y sea el único documento `.docx`. 
2. Ejecutar en una terminal el comando `$ ruby tique_res_es.rb`.
3. Seguir instrucciones.
4. En el documento `tex` arreglar las cabeceras y en el primer párrafo de la reseña quitar la indentación.
5. Compilar desde TexStudio el archivo para obtener el PDF
6. Colocar título de la reseña en `h1` y borrar la etiqueta `<img />`.
