Conversión a XML Jats SPS
=========================

Aviso
-----

Proceso sugerido para la conversión de artículos académicos editados en
*ALHE* a versiones XML Jats bajo el SPS (Scielo Publishing Schema).

Esta forma de edición se trabajó a lo largo de 2018, teniendo diversos
resultados. Los aciertos con este método han sido considerables, pero
más las áreas de oportunidad a desarrollar.

A pesar de que existe la metodología brindada por Scielo, limitada a
MS-Word, se ha investigado y propuesto una forma de llegar a los
resultados necesarios por medios distintos, pensado siempre en que no
todos somos usuarios de Windows o tenemos las características necesarias
para usar la herramienta propuesta por Scielo. Por ello he optado por
usar [Pandoc](https://pandoc.org/) como base de este proceso.

Para lograr esta propuesta se han destinado limitadas horas, muy pocas
realmente, esto se traduce en que el proceso sigue en crecimiento, aún
hay mucho por hacer, sin embargo confío en que este método ayuda a
entender mejor qué se hace y cómo se está trabajando el artículo
editado.

Resulta indispensable tener en mente que esta metodología **no es** una
solución definitiva a la edición XML-JATS, sino un puente en
construcción al que aún le queda muchísimo por hacer.

Los diferentes documentos y referencias en las que está basado este
trabajo son:

-   [pandoc-jats](https://github.com/mfenner/pandoc-jats)
-   [NISO Journal Article Tag Suite (JATS) version
    1.0](https://github.com/ncbi/JATSPreviewStylesheets)
-   [JATS-CSL](https://github.com/MartinPaulEve/JATS-CSL)

Con base en la información anterior, mi cometido ha sido editar y
adaptar a las necesidades que la revista *ALHE* ha tenido.

Entre las tareas truncas que aún se investigan y están por hacerse son
los scripts que permitan faciliar los procesos de conversión y la óptima
edición de los archivos `xslt`, `lua` y `cls` que permitan una
conversión más sencilla.

Dicho esto que considero importante, los pasos que he seguido son los
que a continuación se mencionarán.

##Objetivo

Nuestro objetivo es crear un `xml` de acuerdo al estándar que Scielo
exige, este estándar está basado en el [SPS (Scielo Publishing
Schema)](http://docs.scielo.org/projects/scielo-publishing-schema/pt_BR/1.3-branch/),
basado en [Journal Article Tag
Suite](https://www.niso.org/standards-committees/jats), creado por la
[NISO (National Information Standards
Organization](https://www.niso.org/). Así como una versión para imprenta en formato `PDF`.

# Proceso 

### Requisitos

* Instalar pandoc, se puede descargar desde [este link](https://pandoc.org/installing.html). Una vez instalado comprobar su versión ejecutando en una terminal `$ pandoc -v`. 
* Tener instalado Ruby, puede comprobarlo poniendo en su terminal `$ ruby -v`. En caso de que no esté instalado, descargarlo desde [este link](https://www.ruby-lang.org/es/documentation/installation/).
* Una vez instalado Ruby, instalar la gema [rmagick](https://rubygems.org/gems/rmagick/versions/2.15.4?locale=es).

### Proceso

1.  Asegurarse de que todos los archivos a ocupar se encuentren, el directorio tiene que estar nombrado correctamente y sin espacios o acentos:
    * Artículo en formato `.odt`, `.docx` o `html`.
    * Archivo `.bib`. 
    * Material gráfico (en caso de tenerlo), por ejemplo mapas o gráficas en formatos `.eps`, o cualquier otro formato de imagen.[^1]
2.  Descargar el archivo de Tique de acuerdo al idioma en el que está el artículo, se puede descargar de [este link](https://gitlab.com/paratexto/alhe/tree/master/tique).
3. Ejecutar en una terminal el archivo, según el idioma que se haya descargado, p.e. `$ruby tique_es.rb`. 
4. Se desplegará un menú. Selecciona la opción de acuerdo al proceso en que te encuentres. Si seleccionas un proceso saltándote los anteriores, habrá error. Los procesos son los siguientes: 
    - 1. Preparar Archivos: en este paso Tique creará dos carpetas, `DIGITAL` y `PDF`, hará una copia de los archivos, los renombrará y descargará lo faltante para formar nuestros documentos.
    - 2. Edición de metadata: Tique abrirá el archivo `platilla.yaml`, que se llenará con los archivos del artículo, estos se sacarán **de la copia** del archivo `.docx`.[^2] 

    

#### Observaciones de importancia 

-   La institución del autor se verifica en la página Wayta de scielo
    [Wayta](http://wayta.scielo.org) para asegurarnos que está
    normalizada.
-   Identificar cada citación y marcarla con su llave correspondiente,
    en el caso de que haya sucesión de páginas marcarlo como negritas
    separadas de la llave.
-   Verificar los cuadros en la conversión de los archivos, pues pueden
    presentar errores, en caso de que así sea se puede editar sobre el
    XML directamente con ayuda de un validador, yo uso [Tables
    Generator](https://www.tablesgenerator.com/html_tables).
-   Los bloques de cita en markdown (`>`) no son compatibles con la
    conversión a `xml`, se sugiere marcarlos/resaltarlos para
    identificarlos como cita y una vez se trabaje el `xml` se puedan
    etiquetar correctamente.
-   En el caso de que haya más de un autor y/o institución, en el archivo `plantilla.yaml` se deben repetir los capos correspondientes a los metadatos del autor.



Tras el proceso de conversión, el artículo quedará marcadado en aspectos
muy generales, quedando aún un amplio trabajo por hacer y revisar.[^7]

Los pasos para trabajar en `xml` son los siguientes:

1.  En el directorio "Digital" se encuentra el documento `xml`
    producido, este se abre con el software *Oxygen XML Editor*.
2.  El primer apartado a trabajar es la lista de referencias, se colocan
    en el `<back>` y se editan una a una de forma correcta. Para saber
    los pormenores de la correcta etiquetación o resolver dudas
    específicas se puede consultar la página del
    [SPS](http://docs.scielo.org/projects/scielo-publishing-schema/pt_BR/1.3-branch/).
    -   En caso de que se hayan también editado las referencias que
        están desde el documento `docx` pueden cortarse y pegarlas en un
        documento de algún editor de texto, ya que estos pueden servir
        como referencia en la etiqueta `<mixed-citation/>`.[^8]
    -   Prestar atención a los atributos y etiquetas faltantes en los
        capítulos de libros, así como al atributo `publication-type`,
        que siempre será `book`
3.  El siguiente apartado a trabajar son las notas a pie. Se colocan en
    el `<back>` y se editan de forma correcta. verificar los ID's y
    etiquetas.
4.  En el `<body>` se deben arreglar los vínculos de notas y de
    referencias, verificar bien que los ID's y las referecias se
    correspondan.
    -   En el caso de las referencias, la etiqueta `<xref/>` debe
        abarcar apellido, año y página, en caso de que existan. Si desde
        la marcación `md` se separó la paginación u otro elemento, se
        integra nuevamente para que quede dentro de la etiqueta.
    -   En la conversión, las referencias con más de un autor son
        puestas con `&`, hay que cambiar por `y`.
    -   Se deben etiquetar correctamente todos los llamados a nota.
5.  Se da formato a las tablas y gráficas. Se colocan hipervínculos a
    las menciones dentro del texto a tablas y gráficas.
6.  Aplicar escenario de transformación XSLT con Oxygen.

### Cuarto proceso: creación del HTML 

Para la conversión de nuestro XML se utlizará el estilo
`estiloALHE.xsl`, se puede descargar desde [este
link](https://gitlab.com/paratexto/alhe/blob/master/XML/XML_to_HTML/estiloALHE.xsl).
Asimismo, la hoja de estilo `css` relacionada se encuentra disponible en
[este
link](https://gitlab.com/paratexto/alhe/blob/master/XML/XML_to_HTML/estiloalhehtml.css).

1.  Al aplicar el escenario de transformación con el programa Oxygen se
    copia el resultado a tu editor de texto preferido (p. e. Sublime
    Text, Atom, etc.).
2.  asegurarse de que las etiquetas `<meta/>`, `<link/>`, `<hr/>`,
    `<br/>` y `<img />`.[^9]
3. Se modifica la institución para quitar la información repetida.
4. Se edita la sección de "Clasificación Gel" y "Correspondencia".
5. Edición de meses en las fechas de publicado, aceptado y recibido.

## Notas 

[^1]: Formatos `.jpg`,`.png` o `.gif`.

[^2]: No puede haber ninguna etiqueta dentro de este archivo, únicamente texto.

[^3]: Por alguna razón el documento `md` al convertirlo a `xml` omite el
    espacio natural que hay entre líneas, por lo que es muy importante
    que cada párrafo quede en una sola línea, de lo contrario el
    contenido del texto puede alterarse al convertirlo.

[^4]: Las etiquetas span en la cabecera YAML no deben ir, en caso de
    versalitas pasarlas a Mayúsculas.

[^5]: Jabref es un software de gestión de bibliografía que nos permite
    renombrar las llaves de los archivos de una manera sencilla y
    rápida.

[^7]: Continúo en la evauación para tomar la mejor ruta que nos permita
    ahorrar mucho más trabajo.

[^8]: En caso de que se opte por esta opción es muy importante
    asegurarse de que los datos ingrsados en `<mixed-citation/>` y
    `<element-citation/>` sean los mismos, pues la validación del SPS es
    sensible a cualquier diferencia, sea mayúsculas o minúsculas,
    espacios, etc.

[^9]: A pesar de que en la hoja de estilo `xsl` están correctamente
    cerradas, al transformarlas se modifican las etiquetas, se está
    indagando en este problema para su pronta solución.
