* Asegurar que la carpeta tenga un nombre que identifique sin ambigüedad el artículo (p.e. issn-abreviatura-vol-num-id).
* Comprobar que la bibliografía es correcta en el archivo `.bib`.
* Crear una copia de `tique_es.br` en la carpeta del artículo.
* Asegurarse que se encuentre el material necesario para la formación del artículo.
* Correr el comando `$ ruby tique_es.rb`.
* Los procesos indispensables para trabajar con los archivos son el 1 y 3.
* Al editar el archivo `plantilla.yaml` se sugiere utilizar el codumento `.docx` que está en la carpeta `DIGITAL`, cortar y pegar los datos, para así depurar todos los datos en el documento del artículo.
* No puede haber etiquetas html en el archivo `plantilla.yaml`.
* Todos los *tags* deben estar separados por `;`.
* Cuando se haya convertido el archivo para su marcación (paso 7), se debe tener en cuenta lo siguiente:
  * Que cada párrafo debe estar en una línea, es decir, sin saltos.
  * Que correspondan los identificadores del BibTeX con los marcados en el archivo `doc.md`.
  * Que las referencias que no se hayan marcado con el paso 7 se hagan manualmente.[^1]
  * En caso de que los identificadores de BibTeX no coincidan con la etiqueta en el archivo `doc.md`, consultar el archivo original y editar lo que sea necesario.
  *



[^1]: Se sugiere que se revise meticulosamente el archivo con las herramientas de buscar del editor de textos.
